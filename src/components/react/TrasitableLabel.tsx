import React from "react";
const TypingAnimation: React.FC<{ text: string }> = ({ text }) => {
  const [displayText, setDisplayText] = React.useState('');

  React.useEffect(() => {
    let currentIndex = 0;
    const interval = setInterval(() => {
      if (currentIndex < text.length-1) {
        if(displayText != text) setDisplayText(prevText => prevText + text[currentIndex]);
        currentIndex++;
      } else {
        clearInterval(interval);
      }
    }, 100); // Velocidad de escritura (milisegundos)

    return () => clearInterval(interval); // Limpiar el intervalo al desmontar el componente
  }, [text]);

  return <p className='text-lg font-bold border-b-2 border-gray-500 inline-block"'>{displayText}</p>;
};

export default TypingAnimation;
